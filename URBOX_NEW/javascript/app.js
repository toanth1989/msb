if (typeof BASE_URL === undefined) {
    BASE_URL = window.location.host;
}

let token = _.cookie.get('token');
let site_name = "game";
// Dom7
_.mod = {};
var $$ = Dom7;
// Init App

_.app = new Framework7({
    root: '#app',
    name: 'My App',
    id: 'com.page.game',
    panel: false,

    routes: routes,
    touch: {
        // Enable fast clicks
        fastClicks: true,
    },
    navbar: {
        hideOnPageScroll: true,
        showOnPageScrollEnd: false
    },
    view:{
        reloadPages: true,
        domCache: true,
        history: true,
        iosDynamicNavbar: false,
        pushState: true,
        pushStateRoot: BASE_URL +"",
        pushStateSeparator: "",
        pushStateOnLoad: false
    },
    init: false
});
function copyToClipboard(value) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -1000px; top: -1000px";
    tempInput.value = value;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
}
_.app.on('pageInit', function (page) {
    let show_dialog = $$("#show_dialog").val();
    if(show_dialog === "1") {
      //  _.app.dialog.open("#welcome_dialog");
    }
    if($$('#popup_term_voucher').length){
        $$('#popup_term_voucher').trigger('click');
    }
    if($$('.copyvsv').length){
        $$('.copyvsv').on('click', function () {
            copyToClipboard($$('#COPY_CODE').text());
            _.app.dialog.create({
                cssClass:'copysvscl',
                text: '<div style="text-align:center"><img src="./css/images/urbox/icocopy.svg"/><span style="display: block;color: #969EB2;font-size:17px;">Đã sao chép</span></div>',
                buttons: []
            }).open();
            setTimeout(function () {
                _.app.dialog.close();
            }, 1000);
        });
    }

    $$('.swiper-init-s').each(function (e) {
        var swiper = _.app.swiper.create($$(this), {
            speed: 400,
            spaceBetween: 5,
            pagination: {
                el: '.swiper-pagination',
                type: 'progressbar',
            }
        });
        swiper.on('transitionEnd', function(e) {
            var cr = swiper.realIndex + 1;
            var all = swiper.slidesGrid.length;
            $$('.swiper-init-s').find('.swiper-pagination-page>label').html(cr + '/' + all);
            console.log('*** mySwiper', swiper);
            console.log('*** mySwiper.realIndex', swiper.realIndex);
        });
    });

    $$('.swiper-init-s-2').each(function (e) {
        var swiper = _.app.swiper.create($$(this), {
            speed: 400,
            spaceBetween: 5,
            pagination: {
                el: '.swiper-pagination',
                type: 'progressbar',
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });
        swiper.on('transitionEnd', function(e) {
            var cr = swiper.realIndex + 1;
            var all = swiper.slidesGrid.length;
            $$('.swiper-init-s-2').find('.swiper-pagination-page>label').html(cr + '/' + all);
            console.log('*** mySwiper', swiper);
            console.log('*** mySwiper.realIndex', swiper.realIndex);
        });
    });

    $$('.swiper-init-s-1').each(function (e) {
        var swiper = _.app.swiper.create($$(this), {
            speed: 400,
            spaceBetween: 25,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });
        swiper.on('transitionEnd', function(e) {
            var cr = swiper.realIndex + 1;
            var all = swiper.slidesGrid.length;
            $$('.swiper-init-s-1').find('.swiper-pagination-page>label').html('<span>' + cr + '</span>/' + all);
            console.log('*** mySwiper', swiper);
            console.log('*** mySwiper.realIndex', swiper.realIndex);
        });
    });
});


_.app.init();

_.app.views.create('.view-main', {});
function validateForm(t){
    var form = $$(t)[0];
    var data = new FormData(form);
    var _data = {};
    data.forEach((item, index) => {
        _data[index] = item;
    });
    $$('.warningx').html('Mã PIN không hợp lệ').removeClass('hidden');
    event.preventDefault();return false
}

function validateForm1(t){
    $$('.warningxx.hidden,.warningy.hidden').removeClass('hidden');
    event.preventDefault();return false;
}
function pmc(t,v){
    event.preventDefault();
    var $input = $$(t).closest('.pmc').find('input');
    var $input_val = $input.val();
    if(v === 'plus'){
        ++$input_val;
    }else{
        if($input_val - 1 >= 0){
            --$input_val
        }
    }
    if($input_val === '00' || $input_val == 0){
        $input_val = '00';
    }
    else if($input_val < 10){
        $input_val = '0' + $input_val;
    }
    $input.val($input_val);
}
_.click = {
    link: function (link) {
        _.app.preloader.show();
        _.app.views.main.router.navigate(link, {
            reloadAll: true,
            ignoreCache: true
        });
    },
    register: function () {
        $$('input').removeClass('input-error');
        let phone = $$('#input-phone').val();
        let code = $$('#input-code').val();

        if(code === ''){
            $$('#input-code').focus().addClass("input-error");
            $$("#message-error").removeClass('hidden');
            return false;
        }
        $$('#input-code').removeClass("input-error");


        if(phone === ''){
            $$('#input-phone').focus().addClass("input-error");
            $$("#message-error").removeClass('hidden');
            return false;
        }
        $$('#input-phone').removeClass("input-error");
        if(!_.is_phone(phone)){
            $$('#input-phone').focus().addClass("input-error");
            $$("#message-error").removeClass('hidden');
            return false;
        }
        $$('#input-phone').removeClass("input-error");

        $$("#message-error").addClass('hidden');

        _.app.request({
            url: BASE_URL + "/ajax/wheel/register",
            dataType: 'json', type: "POST",
            data: {phone: phone, code: code},
            success: function (j) {
                if (j.done === 1) {
                    _.redirect('/')
                }
            }
        });

    }
}