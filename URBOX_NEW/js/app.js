if (typeof BASE_URL === undefined) {
    BASE_URL = window.location.host;
}
if (typeof INDEX_ROUTE === undefined) {
    INDEX_ROUTE = BASE_URL + '/';
}
if (typeof REGISTER_ROUTE === undefined) {
    REGISTER_ROUTE = BASE_URL + '/register';
}
if (typeof WHEEL_ROUTE === undefined) {
    WHEEL_ROUTE = BASE_URL + '/wheel';
}
if (typeof DRAW_ROUTE === undefined) {
    DRAW_ROUTE = BASE_URL + '/draw';
}
if (typeof AVAILABLE_GIFTS_ROUTE === undefined) {
    AVAILABLE_GIFTS_ROUTE = BASE_URL + '/available-gifts';
}

let token = _.cookie.get('token');
let site_name = "game";
// Dom7
_.mod = {};
var $$ = Dom7;
// Init App

_.app = new Framework7({
    root: '#app',
    name: 'My App',
    id: 'com.page.game',
    panel: false,

    touch: {
        // Enable fast clicks
        fastClicks: true,
    },
    navbar: {
        hideOnPageScroll: true,
        showOnPageScrollEnd: false
    },
    view: {
        reloadPages: true,
        domCache: true,
        history: true,
        iosDynamicNavbar: false,
        pushState: false,
        pushStateRoot: BASE_URL + "",
        pushStateSeparator: "",
        pushStateOnLoad: false
    },
    init: false
});

_.app.on('pageInit', function (page) {
    let show_dialog = $$("#show_dialog").val();
    if (show_dialog === "1") {
        _.app.dialog.open("#welcome_dialog");
    }
    if ('/' === window.location.pathname) {
        _.app.request({
            url: AVAILABLE_GIFTS_ROUTE,
            dataType: 'json', type: "GET",
            error(xhr, status) {
                console.log('Unable to fetch gifts');
            },
            success(data, status, xhr) {
                if (200 === status) {
                    _.cookie.set('AVAILABLE_GIFTS', JSON.stringify(data.data));
                }
            }
        });
    }
    if ('/wheel' === window.location.pathname) {
        LuckyWheel.init();
    }
});

_.app.init();

_.app.views.create('.view-main', {});

_.click = {
    link: function (link) {
        _.app.preloader.show();
        _.app.views.main.router.navigate(link, {
            reloadAll: true,
            ignoreCache: true
        });
    },
    register: function () {
        $$('input').removeClass('input-error');
        let phone = $$('#input-phone').val();
        let code = $$('#input-code').val();
        let error = 0;
        if (code === '') {
            $$('#input-code').focus().addClass("input-error");
            $$('#label-input-code').focus().addClass("color-gray");
            $$("#message-error").removeClass('hidden');
            error = 1;
        }


        if (phone === '') {
            $$('#input-phone').focus().addClass("input-error");
            $$('#label-input-phone').focus().addClass("color-gray");
            $$("#message-error").removeClass('hidden');
            error = 1;
        }

        if (!_.is_phone(phone)) {
            $$('#input-phone').focus().addClass("input-error");
            $$('#label-input-phone').focus().addClass("color-gray");
            $$("#message-error").removeClass('hidden');
            error = 1;
        }
        if(error === 1) {
            return false;
        }
        $$('#label-input-code').focus().removeClass("color-gray");
        $$('#input-code').removeClass("input-error");
        $$('#input-phone').removeClass("input-error");
        $$('#label-input-phone').focus().removeClass("color-gray");
        $$("#message-error").addClass('hidden');


        _.app.request({
            url: REGISTER_ROUTE,
            dataType: 'json', type: "POST",
            data: {phone: phone, code: code, _token: _.getCSRFToken()},
            error(xhr, status) {
                _.cookie.set('CURRENT_CODE', '');
                _.cookie.set('CURRENT_PHONE', '');
                $$("#message-error").removeClass('hidden');

                $$('#input-code').focus().addClass("input-error");
                $$('#label-input-code').focus().addClass("color-gray");

                $$('#input-phone').focus().addClass("input-error");
                $$('#label-input-phone').focus().addClass("color-gray");

                let data = JSON.parse(xhr.response);
                $$("#message-error").html(data.message);
            },
            success(data, status, xhr) {
                _.cookie.set('CURRENT_CODE', code);
                _.cookie.set('CURRENT_PHONE', phone);
                _.cookie.set('WHEEL_STOP_POSITION', data.position);
                if (200 === status) {
                    _.redirect(WHEEL_ROUTE);
                }
            }
        });
    }
}
