let currentPhone = _.cookie.get('CURRENT_PHONE');
let currentCode = _.cookie.get('CURRENT_CODE');
let WHEEL_STOP_POSITION = _.cookie.get('WHEEL_STOP_POSITION');
if(WHEEL_STOP_POSITION == null || WHEEL_STOP_POSITION == ""){
    WHEEL_STOP_POSITION = 0;
}
WHEEL_STOP_POSITION = parseInt(WHEEL_STOP_POSITION);
if(currentCode == null || currentPhone == null){
    //window.location.href = INDEX_ROUTE;
}

let LuckyWheel = {
    theWheel: undefined,
    wheelSpinning: false,
    audio: new Audio('./js/tick.mp3'),

    segments: function () {
        let gifts = _.cookie.get('AVAILABLE_GIFTS');
        gifts = JSON.parse(gifts);
        let fillStyles = [
            '#e70697', '#fff200', '#f6989d', '#ee1c24', '#3cb878', '#000000',
        ];
        let segments = [];
        for (let i = 0; i < gifts.length; i++) {
            let gift = gifts[i];
            let segment = {
                fillStyle: fillStyles[i],
                text: gifts[i]['title'],
                textFontSize: 10
            };
            segments.push(segment);
        }

        return segments;
    },

    init: function () {
        LuckyWheel.theWheel = new Winwheel({
            'outerRadius': 147,        // Set outer radius so wheel fits inside the background.
            'innerRadius': 23,         // Make wheel hollow so segments dont go all way to center.
            'textFontSize': 10,         // Set default font size for the segments.
            'textOrientation': 'curved', // Make text vertial so goes down from the outside of wheel.
            'textAlignment': 'outer',    // Align text to outside of wheel.
            'textMargin': 15,
            'numSegments': 6,         // Specify number of segments.
            'lineWidth': 0,
            'drawMode': 'image',
            'textFontFamily': 'Roboto',
            'strokeStyle': 'transparent',

            'drawText': false,
            'responsive': false,
            'segments':
                [
                    {
                        'text': 'Quạt máy Misubishi'
                    },
                    {
                        'text': 'Chúc bạn may mắn lần sau'
                    },
                    {
                        'text': 'Điện thoại Samsung Galaxy A20'
                    },
                    {
                        'text': 'Thẻ quà tặng 200.000 đ'
                    },
                    {
                        'text': 'Máy lọc nước Unilever Puriet Casa'
                    },
                    {
                        'text': 'Chúc bạn may mắn lần sau'
                    }
                ],
            'animation':           // Specify the animation to use.
                {
                    'stopAngle': WHEEL_STOP_POSITION, // vị trí dừng lại, 360/6 phần  0-60,60-120,120-180,180-240,240-280,280-360
                    'type': 'spinToStop',
                    'duration': 10,
                    'spins': 3,
                    'callbackFinished': LuckyWheel.alertPrize,  // Function to call whent the spinning has stopped.
                    'callbackSound': LuckyWheel.playSound,      // Called when the tick sound is to be played.
                    'soundTrigger': 'pin'                       // Specify pins are to trigger the sound.
                },
            'pins':                // Turn pins on.
                {
                    'number': 0,
                    'fillStyle': 'white',
                    'outerRadius': 4,
                }
        });
        let image = new Image();

        image.src = "../css/images/inner-wheel.svg";
        image.width = 294;
        image.height = 294;
        image.onload = function () {
            LuckyWheel.theWheel.wheelImage = image; // Make wheelImage equal the loaded image object.
            LuckyWheel.theWheel.draw();                        // Also call draw function to render the wheel.
        }
    },

    resetSpin: function () {
        LuckyWheel.theWheel.stopAnimation(false); // Stop the animation, false as param so does not call callback function.
        LuckyWheel.theWheel.rotationAngle = 0;               // Re-set the wheel angle to 0 degrees.
        LuckyWheel.theWheel.draw();                          // Call draw to render changes to the wheel.
        LuckyWheel.wheelSpinning = false;                    // Reset to false to power buttons and spin can be clicked again.
    },

    playSound: function () {
        LuckyWheel.audio.pause();
        LuckyWheel.audio.currentTime = 0;
        LuckyWheel.audio.play();
    },

    alertPrize: function (indicatedSegment) {
        //console.log(indicatedSegment)
        _.app.request({
            url: DRAW_ROUTE,
            data: {_token: _.getCSRFToken()},
            dataType: 'json', type: "post",
            beforeSend: function () {
              //  _.app.preloader.show();
            },
            complete: function () {
               // _.app.preloader.hide();
            },
            success: function (a) {
                _.cookie.set('CURRENT_CODE', '');
                _.cookie.set('WHEEL_STOP_POSITION', '');
                _.cookie.set('CURRENT_PHONE', '');
                $$("#btn-start-wheel").addClass("hidden");

               let html = `
                <div class="text-align-center">
                       <h3 class="f22 f-500">${a.gift.title}</h3>
                       <p class="f15">${a.gift.name}</p>
                       <p class="f11">${a.gift.note}</p>
                       <div class="mT20 mB20">
                            <a href="/" class="btn btn-active w100p external">Tiếp tục đổi quà</a>
                        </div>
                </div>
               `;
                _.app.dialog.create({
                    title: false,
                    text: html
                }).open();
            }
        });
    },

    startSpin: function () {
        // Ensure that spinning can't be clicked again while already running.
        if (LuckyWheel.wheelSpinning === false) {
            // Based on the power level selected adjust the number of spins for the wheel, the more times is has
            LuckyWheel.theWheel.animation.spins = 15;
            // Disable the spin button so can't click again while wheel is spinning.
            // Begin the spin animation by calling startAnimation on the wheel object.
            LuckyWheel.theWheel.startAnimation();
            // Set to true so that power can't be changed and spin button re-enabled during
            // the current animation. The user will have to reset before spinning again.
            LuckyWheel.wheelSpinning = true;
        }
    }
};
