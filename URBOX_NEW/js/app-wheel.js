let theWheel = new Winwheel({
    'outerRadius'     : 147,        // Set outer radius so wheel fits inside the background.
    'innerRadius'     : 23,         // Make wheel hollow so segments dont go all way to center.
    'textFontSize'    : 10,         // Set default font size for the segments.
    'textOrientation' : 'curved', // Make text vertial so goes down from the outside of wheel.
    'textAlignment'   : 'outer',    // Align text to outside of wheel.
    'textMargin'     : 15,
    'numSegments'     : 6,         // Specify number of segments.
    'lineWidth'       : 0,
    'drawMode'          : 'image',
    'textFontFamily'    : 'Roboto',
    'strokeStyle'      :'transparent',

    'drawText'          : false,
    'responsive'        : true,
    'segments'        :
        [
            {'text' : 'Máy lọc nước\n' +
                    'Unilever Puriet\n' +
                    'Casa'},
            {'text' : 'Thẻ quà tặng\n' +
                    '200.000 đ'},
            {'text' : 'Điện thoại\n' +
                    'Samsung\n' +
                    'Galaxy A20'},
            {'text' : 'Máy lọc nước\n' +
                    'Unilever Puriet\n' +
                    'Casa'},
            {'text' : 'Thẻ quà tặng\n' +
                    '200.000 đ'},
            {'text' : 'Điện thoại\n' +
                    'Samsung\n' +
                    'Galaxy A20'}
        ],
    'animation' :           // Specify the animation to use.
        {
            'stopAngle' :3000,
            'type'     : 'spinToStop',
            'duration' : 10,
            'spins'    : 8,
            'callbackFinished' : alertPrize,  // Function to call whent the spinning has stopped.
            'callbackSound'    : playSound,   // Called when the tick sound is to be played.
            'soundTrigger'     : 'pin'        // Specify pins are to trigger the sound.
        },
    'pins' :                // Turn pins on.
        {
            'number'     : 0,
            'fillStyle'  : 'white',
            'outerRadius': 4,
        }
});

let wheelSpinning = false;
function startSpin() {
    // Ensure that spinning can't be clicked again while already running.
    if (wheelSpinning === false) {
        // Based on the power level selected adjust the number of spins for the wheel, the more times is has
        theWheel.animation.spins = 15;
        // Disable the spin button so can't click again while wheel is spinning.
        // Begin the spin animation by calling startAnimation on the wheel object.
        theWheel.startAnimation();
        // Set to true so that power can't be changed and spin button re-enabled during
        // the current animation. The user will have to reset before spinning again.
        wheelSpinning = true;
    }
}

function resetSpin() {
    theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
    theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
    theWheel.draw();                // Call draw to render changes to the wheel.
    wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
}
let audio = new Audio('./javascript/tick.mp3');
function playSound() {
    audio.pause();
    audio.currentTime = 0;
    audio.play();
}

function alertPrize(indicatedSegment)
{
    _.app.dialog.alert(indicatedSegment.text,"")
}
// Create new image object in memory.
let loadedImg = new Image();

// Create callback to execute once the image has finished loading.
loadedImg.onload = function()
{
    theWheel.wheelImage = loadedImg;    // Make wheelImage equal the loaded image object.
    theWheel.draw();                    // Also call draw function to render the wheel.
}

// Set the image source, once complete this will trigger the onLoad callback (above).
loadedImg.src = "./css/images/inner-wheel.svg";
loadedImg.width = 294;
loadedImg.height = 294;
// Called when the animation has finished.
function alertPrize(indicatedSegment)
{
    // Do basic alert of the segment text.
    alert("The plane is " + indicatedSegment.text);
}
