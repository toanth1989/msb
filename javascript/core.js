/*-----------------------------*/
/*        SHOPPING CORE		   */
/*-----------------------------*/
var _ = {
	_store : {
		ajax: {},
		method: {},
		variable: {},
		cache:{}
	},
};

//token key
_.getCSRFToken = function() {if(document.domain!=DOMAIN_NAME){return '0102yuh'}return BASE_TOKEN};

_.popup = function(popup_id, title, content, option) {
	var html = 
	'<div class="popup debug-popup" style="width: 100%; max-width:512px;">' +
		'<div id="jsPopup'+popup_id+'" class="view navbar-fixed">' +
			'<div class="pages">' +
				'<div class="page">' +
					'<div class="navbar">' +
						'<div class="navbar-inner">' +
							'<div class="left"><a href="#" class="link close-popup">Đóng lại</a></div>' +
							'<div class="center">'+ title +'</div>' +
							'<div class="right">'+ ((option.action != undefined)? option.action : '') +'</div>' +
						'</div>' +
					'</div>' +
					'<div class="page-content bgGray">' +
						'<div class="p15">' +
							'<div class="card">' +
								content +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>' +
	'</div>';
	_.app.popup(html, true, true);
};

/*-----------------------------*/
/*        CHECKING			   */
/*-----------------------------*/

_.is_arr = function(arr) { return (arr != null && arr.constructor == Array) };
_.is_str = function(str) { return (str && (/string/).test(typeof str)) };
_.is_func = function(func) { return (func != null && func.constructor == Function) };
_.is_num = function(num) { var num = Number(num); return (num != null && !isNaN(num)) };
_.is_int = function (x) {var y=parseInt(x);if (isNaN(y)) return false;return x==y && x.toString()==y.toString();} 
_.is_obj = function(obj) { return (obj != null && obj instanceof Object) };
_.is_ele = function(ele) { return (ele && ele.tagName && ele.nodeType == 1) };
_.is_exists = function(obj) { return (obj != null && obj != undefined && obj != "undefined") };
_.is_blank = function(str) { return (_.trim(str) == "") };
_.is_phone = function(num) {return (/^(01([0-9]{2})|09[0-9]|08[0-9]|03[0-9]|05[0-9]|07[0-9]|08[0-9])(\d{7})$/i).test(num)};
_.is_email = function(str) {return (/^[a-z-_0-9\.]+@[a-z-_=>0-9\.]+\.[a-z]{2,3}$/i).test(_.trim(str))};
_.is_username = function(value){ return (value.match(/^[0-9]/) == null) && (value.search(/^[0-9_a-zA-Z]*$/) > -1); }
_.is_link = function(str){ return (/(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/).test(_.trim(str)) };
_.is_image = function(imagePath){
	var fileType = imagePath.substring(imagePath.lastIndexOf("."),imagePath.length).toLowerCase();
	return (fileType == ".gif") || (fileType == ".jpg") || (fileType == ".png") || (fileType == ".jpeg");
};
_.is_ff  = function(){ return (/Firefox/).test(navigator.userAgent) };
_.is_ie  = function() { return (/MSIE/).test(navigator.userAgent) };
_.is_ie6 = function() { return (/MSIE 6/).test(navigator.userAgent) };
_.is_ie7 = function() { return (/MSIE 7/).test(navigator.userAgent) };
_.is_ie8 = function() { return (/MSIE 8/).test(navigator.userAgent) };
_.is_chrome = function(){ return (/Chrome/).test(navigator.userAgent) };
_.is_opera = function() { return (/Opera/).test(navigator.userAgent) };
_.is_safari = function(){ return (/Safari/).test(navigator.userAgent) };
_.isAdminUrl = function(){return _.is_exists(URL_PARAMS.page) && (URL_PARAMS.page == 'admin' || URL_PARAMS.page == 'edit_page' || URL_PARAMS.page == 'module' || URL_PARAMS.page == 'themes' || URL_PARAMS.page == 'page'); };

/*-----------------------------*/
/*        CACHE & DATA  	   */
/*-----------------------------*/
_.cache = {
	get: function (key, def) {if(_.is_exists(_._store.cache[key])){return _._store.cache[key]; } return _.is_exists(def) ? def : ''; },
	set: function (key, val) {_._store.cache[key] = val; },
	del: function(key){_._store.cache[key] = null; delete _._store.cache[key]; }
};

/*-----------------------------*/
/*        WORKING COOKIE	   */
/*-----------------------------*/

_.cookie = {
	mode:0, //0: default, 1: no COOKIE_ID
	set:function(name,value,expires,path,domain,secure){expires instanceof Date?expires=expires.toGMTString():typeof(expires)=='number'&&(expires=(new Date(+(new Date)+expires*1e3)).toGMTString());if(_.cookie.mode){var r=[name+"="+escape(value)],s,i}else{var r=[COOKIE_ID+'_'+name+"="+escape(value)],s,i}if(domain==undefined&&DOMAIN_COOKIE_REG_VALUE>0){domain=DOMAIN_COOKIE_STRING}if(path==undefined){path='/'}for(i in s={expires:expires,path:path,domain:domain}){s[i]&&r.push(i+"="+s[i])}return secure&&r.push("secure"),document.cookie=r.join(";"),true},
	get:function(a){if(document.cookie.length>0){if(_.cookie.mode==0){a=COOKIE_ID+'_'+a}c_start=document.cookie.indexOf(a+"=");if(c_start!=-1){c_start=c_start+a.length+1;c_end=document.cookie.indexOf(";",c_start);if(c_end==-1)c_end=document.cookie.length;return unescape(document.cookie.substring(c_start,c_end))}}return""}
};

/*-----------------------------*/
/*        TOOLS				   */
/*-----------------------------*/

/* function core connect */
String.prototype.E = function() {return _.get_ele(this)}; // var obj = ('ads_zone2').E()

// join string to make theme
_.join = function(b){var c=[b];return function extend(a){if(a!=null&&'string'==typeof a){c.push(a);return extend}return c.join('')}};
_.trim = function(str) {return (/string/).test(typeof str) ? str.replace(/^\s+|\s+$/g, "") : ""};
_.util_random = function(a, b) { return Math.floor(Math.random() * (b - a + 1)) + a };
_.get_ele = function(id) { return document.getElementById(id) };
_.id = function() { return (new Date().getTime() + Math.random().toString().substring(2)) + '_'};
_.get_form = function(a){var b=_.get_ele(a);if(!_.is_ele(b))return'';var c=[],inputs=b.getElementsByTagName("input");for(var i=0;i<inputs.length;i++){var d=inputs[i];if(d.type!='button'){c.push(d.name+"="+encodeURIComponent(d.value))}}var e=b.getElementsByTagName("select");for(var i=0;i<e.length;i++){var d=e[i],key=d.name,value=d.options[d.selectedIndex].value;c.push(key+"="+encodeURIComponent(value))}var f=b.getElementsByTagName("textarea");for(var i=0;i<f.length;i++){var d=f[i];c.push(d.name+"="+encodeURIComponent(d.value))}return c.join("&")};

/*-----------------------------*/
/*        EXTRA FUNCTIONS	   */
/*-----------------------------*/

//redirect to url
_.redirect = function(url, is_new){if(url != ''){if(is_new){window.open(url); }else{window.location = url; } } };

//reload page
_.reload = function(){window.location.reload()};
_.deleteCache = function(a,b){_.ajax_popup("act=admin&code=delcache",'POST',{cacheKey:a,cacheDir:b},function(j){if(j.err==0){$$('#cache-con-'+j.hashKey).remove();_.systemAlert(_.join('<div style="font-size:14px;margin-top:5px">Xoá cache thành công</div>')(),'Hệ thống',2000)}})};
_.showCache = function(a,c,b){_.ajax_popup("act=admin&code=showCache",'POST',{cacheKey:a,cacheDir:b,cacheTime:c},function(j){if(j.err==0){$$("#showValue"+j.hashKey).html(prettyPrint(j.msg)).show()}else{_.systemAlert(_.join('<div style="font-size:14px;margin-top:5px">'+j.msg+'</div>')(),'Hệ thống',2000)}})};
_.auto_scroll = function(anchor){var target=$$(anchor);target=target.length&&target||$$('[name='+anchor.slice(1)+']');if(target.length){var targetOffset=target.offset().top;$$('html,body').animate({scrollTop:targetOffset},1000);return false}return true};
//input number only
_.numberOnly = function(myfield,e){var key,keychar;if(window.event){key=window.event.keyCode}else if(e){key=e.which}else{return true}keychar=String.fromCharCode(key);if((key==null)||(key==0)||(key==8)||(key==9)||(key==13)||(key==27)){return true}else if(("0123456789").indexOf(keychar)>-1){return true}return false};

_.enter = function(id,cb){if(cb){if(!_.is_exists(_._store.variable['key_listener'])){_._store.variable['key_listener']=0}$$(id).keydown(function(event){if(event.keyCode==13){_._store.variable['key_listener']=setTimeout(cb,10)}else{clearTimeout(_._store.variable['key_listener'])}})}};
_.numberFormat = function(number,decimals,dec_point,thousands_sep){var n=number,prec=decimals;n=!isFinite(+n)?0:+n;prec=!isFinite(+prec)?0:Math.abs(prec);var sep=(typeof thousands_sep=="undefined")?'.':thousands_sep;var dec=(typeof dec_point=="undefined")?',':dec_point;var s=(prec>0)?n.toFixed(prec):Math.round(n).toFixed(prec);var abs=Math.abs(n).toFixed(prec);var _,i;if(abs>=1000){_=abs.split(/\D/);i=_[0].length%3||3;_[0]=s.slice(0,i+(n<0))+_[0].slice(i).replace(/(\d{3})/g,sep+'$1');s=_.join(dec)}else{s=s.replace(',',dec)}return s};
_.selectAllText = function(o){o.focus();o.select()};

//thay doi phien ban PC & web
_.changeMode = function (is_pc, redirect){
	var mode = is_pc ? 'pc' : 'mobile', key = 'websiteMode';
	_.cookie.set(key, mode, 86400*365, '/');
	if(redirect && _.is_link(redirect)){
		_.redirect(redirect)
	}else{
		_.reload();
	}
};

_.stopSubmitOnEnter = function(e) {var eve = e || window.event; var keycode = eve.keyCode || eve.which || eve.charCode; if (keycode == 13) {eve.cancelBubble = true; eve.returnValue = false; if (eve.stopPropagation) {eve.stopPropagation(); eve.preventDefault(); } return false; } }


_.getParams = function(key, def) {
	qs = document.location.search;
	qs = qs.split('+').join(' ');

	var params = [],
		tokens,
		re = /[?&]?([^=]+)=([^&]*)/g;

	while (tokens = re.exec(qs)) {
		params.push({
			key: decodeURIComponent(tokens[1]),
			val: decodeURIComponent(tokens[2])
		});
	}

	// Lấy thêm tham số từ path
	var p = window.location.pathname;
		p = p.replace('/urinte', '');
	p = p.slice(1, -5);
	p = p.split('/');
	params.push({key: 'page', val: p[0]});
	if(p[2] != undefined) {
		if(p[2] == 'search' || p[2] == 'group'){
			params.push({key: 'action', val: p[2]});	
		}
		else {
			if(p[1]=='call'){
				params.push({key: 'contact_id', val: p[2]});
			}else{
				params.push({key: 'id', val: p[2]});
			}
		}
	}

	if(key != undefined){
		for(x in params){
			if(key == params[x].key){
				return params[x].val;
			}
		}
		return def;
	}
	else return params;
}

_.getUrl = function(base){
	var url = '';
	var params = _.getParams();
	var queryString = '';

	for(x in params){
		if(params[x].key != 'page_no' && params[x].key != 'id' && params[x].key != undefined) {
			if(queryString != '?') queryString += '&';
			queryString += params[x].key + '=' + params[x].val;
		}
	}
	url += queryString;
	return encodeURI(url);
}

_.getFormVal = function(form_id){
	var formElements=document.getElementById(form_id).elements;
	var postData={};
	for (var i=0; i<formElements.length; i++){
		if (formElements[i].type != "submit" && !_.is_blank(formElements[i].name)){//we dont want to include the submit-buttom
			if(formElements[i].type == "checkbox"){
				if(formElements[i].checked) postData[formElements[i].name]=formElements[i].value;
			}else if(formElements[i].type == "radio"){
				if(formElements[i].name.indexOf('[]') > 1){
					var name = formElements[i].name.replace('[]', '');
					if(_.is_arr(postData[name])){
						if(formElements[i].checked) postData[name].push(formElements[i].value);
					}else{
						//temp = postData[name];
						if(formElements[i].checked)  postData[name] = new Array(formElements[i].value);
					}
				}
				else{
					if(formElements[i].checked) postData[formElements[i].name]=formElements[i].value;
				}
			}else if(formElements[i].type == "file"){
				postData[formElements[i].name]=formElements[i].files[0];
			}
			else{
				if(formElements[i].name.indexOf('[]') > 1){
					var name = formElements[i].name.replace('[]', '');
					if(_.is_arr(postData[name])){
						postData[name].push(formElements[i].value);
					}else{
						//temp = postData[name];
						postData[name] = new Array(formElements[i].value);
					}
				}
				else{
					postData[formElements[i].name]=formElements[i].value;
				}
			}
		}
	}
	return postData;
}

//for debug
_.debug = function(a){$$('body').append(prettyPrint(a))};

//auto run
_.ready={func:{'web':[],'admin':[]},add:function(cb,admin){if(admin){_.ready.func.admin[_.ready.func.admin.length]=cb}else{_.ready.func.web[_.ready.func.web.length]=cb}},run:function(){if(_.isAdminUrl()){if(_.ready.func.admin.length>0){for(var i in _.ready.func.admin){_.ready.func.admin[i]()}}}else{if(_.ready.func.web.length>0){for(var i in _.ready.func.web){_.ready.func.web[i]()}}}}};

_.admin = {};

function chunkArray(myArray, chunk_size) {
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];

    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index + chunk_size);
        // Do something if you want with the group
        tempArray.push(myChunk);
    }

    return tempArray;
}