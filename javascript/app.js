if (typeof BASE_URL == undefined) {
      BASE_URL = window.location.host;
}
var BASE_URL = window.location.host;
// Dom7
_.mod = {}
var $$ = Dom7;
_.app = new Framework7({
    root: '#app',
    name: 'My App',
    id: 'com.urbox.viettel',
    panel: false,
    routes: routes,
    touch: {
        // Enable fast clicks
        fastClicks: true,
    },
    init: false
});
_.app.on('pageInit', function (page) {
    // Page Data contains all required information about loaded and initialized page
    _.app.searchbar.create({
        el: '.searchbar',
        searchContainer: '.list',
        searchIn: '.item-title',
        on: {
            search(sb, query, previousQuery) {
                console.log(query, previousQuery);
            }
        }
    });
    $$(".searchbar-disable-button").on("click",function () {
        $$(".searchbar-found").css("display","none");
    });

    $$('.swiper-container').each(function () {
        var slidesPerView = $$(this).data('item-number');
        slidesPerView = slidesPerView || 1;
        var swiper = new Swiper(this, {
            slidesPerView: slidesPerView,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    });


    $$('#TOGGLE_DESCRIPTION').click(function () {
        $$(this).parents('.right').toggleClass('shortdes_hidden');
    })

    var placeholder = 'Nhập họ và tên\ndòng 2\ndòng 3';
    $$('textarea.form-control').val(placeholder);

    $$('textarea.form-control').focus(function(){
        $$('textarea.form-control').addClass('active');
        if($$(this).val() === placeholder){
            $$(this).val('');
        }
    });

    $$('textarea.form-control').blur(function(){
        if($$(this).val() ===''){
            $$('textarea.form-control').removeClass('active');
            $$(this).val(placeholder);
        }
    });
    $$('.btn_add').on('click',function () {
        var input_quantity = $$('.input_quantity').val();
        $$('.input_quantity').val(parseInt(input_quantity) + 1);
    });
    $$('.btn_minus').on('click',function () {
        var input_quantity = $$('.input_quantity').val();
        $$('.input_quantity').val(parseInt(input_quantity) - 1);
    });
    $$(window).scroll(function() {
        if ($$(this).scrollTop()) {
            $$('#toTop').fadeIn();
        } else {
            $$('#toTop').fadeOut();
        }
    });
    $$('#toTop').click(function () {
        $$('.page').animate({scrollTop: 200}, 1000);
    })
});
$$(document).on('input[type="radio"]','change click', function(ev){
    console.log($$(ev.currentTarget).val());
});
$$(document).on('click', '#del-cart-item', function (e) {
    $$(this).hide();
    $$("#fn-cart-item").show();
    $$(".cart_del_item").css("display","inline-block");
    $$("#btn-confirm").removeClass("btn-active").addClass("btn-gray").css("pointer-events","none");

});
$$(document).on('click', '#fn-cart-item', function (e) {
    $$(this).hide();
    $$("#del-cart-item").show();
    $$("#btn-confirm").removeClass("btn-gray").addClass("btn-active").css("pointer-events","");
    $$(".cart_del_item").css("display","none");

});
$$(document).on('change keyup',"#input-phone",function () {
    $$("#confirm-phone").removeClass('btn-gray').addClass('btn-active');
});
_.app.init();
_.app.views.create('.view-main', {
    url: '/',
    dynamicNavbar: true
});



let downloadTimer = null;

_.mod.onSetupPhone={
    getPhone:function () {
        let phone_number = $$("#input-phone").val();
        let token = $$("#token-input").val();
        if(phone_number === "" || !_.is_phone(phone_number)){
            $$("#phone_error").removeClass('hidden');
            $$("#input-phone").addClass('input-error');
            return false;
        }
        $$("#phone_error").addClass('hidden');
        $$("#input-phone").removeClass('input-error');
        _.app.request({
            url: "https://api.ipify.org?format=json",
            data: {phone:phone_number,token:token},
            dataType: 'json', type: "get",
            beforeSend:function(){
                _.app.preloader.show();
            },
            complete:function(){
                _.app.preloader.hide();
            },
            success: function (a) {
                    _.app.request({
                        url: "https://api.ipify.org?format=json",
                        data: {token: token,phone:phone_number},
                        dataType: 'json', type: "get",
                        beforeSend:function(){
                            _.app.preloader.show();
                        },
                        complete:function(){
                            _.app.preloader.hide();
                        },
                        success: function (a) {
                            $$('#message_otp').html('Mã OTP mới sẽ được gửi lại sau <span id="countdowntime">60</span> giây').removeClass('hidden');
                            var timeleft = 60;
                            downloadTimer = setInterval(function(){
                                timeleft--;
                                document.getElementById("countdowntime").innerHTML = timeleft;
                                if(timeleft <= 0){
                                    $$('#btn-validate').hide();
                                    $$('#message_otp').hide();
                                    $$('#btn-resend').show();
                                    clearInterval(downloadTimer);
                                }
                            },1000);
                        }

                    });
                    let html_gift = "";
                    html_gift += "<div class='pT10'>";
                    html_gift += "<p class='display-block text-align-center f14 mB20'>Vui lòng nhập mã OTP gửi đến số điện thoại <span class='color-spruce f-xbold'> "+phone_number+"</span></p>";
                    html_gift += "<input id='pincode-input' value='' maxlength='6' />";
                    html_gift += "<div id='message_otp' class='f12 italic message-otp text-align-center mT5'></div>";
                    html_gift += "<div id='message_otp2' class='f12 italic message-otp text-align-center mT5 hidden'></div>";
                    html_gift += "<div id='btn-validate' class='mB5 center-text mT30 pT5'><a id='link_otp_btn' href='javascript:void(0)' onclick=\"_.mod.onSetupPhone.validateOtp('"+token+"','"+phone_number+"')\" class='btn btn-gray w100p'>Xác nhận</a></div>";
                    html_gift += "<div id='btn-resend' class='mB5 center-text mT30 hidden'><a id='link_otp_btn_resend' href='javascript:void(0)' onclick=\"_.mod.onSetupPhone.resendOtp('"+token+"','"+phone_number+"')\" class='btn btn-active w100p'>Gửi lại</a></div>";
                    html_gift += "</div>";
                    _.app.dialog.create({
                        title: false,
                        cssClass: "opt_dialog",
                        text: html_gift
                    }).open();

                    _.app.pinInput.init("pincode-input");
            }
        });
    },
};
