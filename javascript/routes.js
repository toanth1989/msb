let routes = [
    // Index page
    {
        path: '/',
        url: './module/popup_xac_nhan_doi_qua_cash-back.html',
        name: 'catalog',
    },{
        path: '/',
        url: './module/mobile_category.html',
        name: 'catalog',
    },{
        path: '/',
        url: './module/mobile_home.html',
        name: 'catalog',
    },{
        path: '/',
        url: './module/popup_xac_nhan_doi_qua_cash-back.html',
        name: 'catalog',
    },{
        path: '/1',
        url: './module/thong_tin_giao_hang_fail.html',
        name: 'catalog',
    },{
        path: '/1',
        url: './module/thong_tin_giao_hang.html',
        name: 'catalog',
    },{
        path: '/1',
        url: './module/detail_qua_cua_toi_dining.html',
        name: 'catalog',
    },{
        path: '/1',
        url: './module/qua_cua_toi.html',
        name: 'catalog',
    },
    {
        path: '/1',
        url: './module/detail_accor_dudk.html',
        name: 'catalog',
    },
    {
        path: '/1',
        url: './module/detail_accor.html',
        name: 'catalog',
    },
    {
        path: '/',
        url: './module/detail_gift_dudk.html',
        name: 'catalog',
    },
    {
        path: '/',
        url: './module/detail_gift.html',
        name: 'catalog',
    },
    {
        path: '/',
        url: './module/detail_cashback_dudk.html',
        name: 'catalog',
    },
    {
        path: '/',
        url: './module/category_voucher_dining_pending.html',
        name: 'catalog',
    },{
        path: '/s',
        url: './module/catalog.html',
        name: 'catalog',
    },    {
        path: '/',
        url: './module/home.html',
        name: 'home',
    },
    {
        path: '/catalog',
        url: './module/catalog.html',
        name: 'catalog',
    },
    {
        path: '/catalog-grid',
        url: './module/catalog-grid.html',
        name: 'catalog-grid',
    },
    {
        path: '/brand',
        url: './module/brand.html',
        name: 'brand',
    },
    {
        path: '/brand-like',
        url: './module/brand-like.html',
        name: 'brand',
    },
    {
        path: '/brand-product',
        url: './module/brand-product.html',
        name: 'brand',
    },
    {
        path: '/brand-discount',
        url: './module/brand-discount.html',
        name: 'brand',
    },
    {
        path: '/brand-cart',
        url: './module/brand-cart.html',
        name: 'brand',
    },
    {
        path: '/brand-store',
        url: './module/brand-store.html',
        name: 'brand',
    },
    {
        path: '/gift',
        url: './module/gift.html',
        name: 'gift',
    },
    {
        path: '/gift-exp',
        url: './module/gift-exp.html',
        name: 'gift',
    },
    {
        path: '/gift-used',
        url: './module/gift-used.html',
        name: 'gift',
    },
    {
        path: '/gift-send',
        url: './module/gift-send.html',
        name: 'gift',
    },
    {
        path: '/welcome',
        url: './module/welcome.html',
        name: 'welcome',
    },
    {
        path: '/onboarding',
        url: './module/onboarding1.html',
        name: 'onboarding',
    },
    {
        path: '/onboarding2',
        url: './module/onboarding2.html',
        name: 'onboarding',
    },
    {
        path: '/1',
        url: './module/guide.html',
        name: 'guide',
    },
];